[Current]
^^^^^^^^^

-  `190ef63 <../../commit/190ef63>`__ - **(dvenkatsagar)** Added
   Changelog. vpat++
-  `4e86c37 <../../commit/4e86c37>`__ - **(dvenkatsagar)** Updated
   Version

0.1.5
^^^^^

-  `3599309 <../../commit/3599309>`__ - **(dvenkatsagar)** Added Help
   feature. vpat++
-  `1cb7582 <../../commit/1cb7582>`__ - **(dvenkatsagar)** Fixed some
   Minor bugs
-  `266a814 <../../commit/266a814>`__ - **(dvenkatsagar)** Update HTML
   language support
-  `765ca27 <../../commit/765ca27>`__ - **(dvenkatsagar)** Updated
   version

0.1.4
^^^^^

-  `62ac88e <../../commit/62ac88e>`__ - **(dvenkatsagar)** Update HTML
   Language Support, Fixed Plain Text Support. vpat++
-  `f2daa92 <../../commit/f2daa92>`__ - **(dvenkatsagar)** fixed main.js
-  `2fdd11b <../../commit/2fdd11b>`__ - **(dvenkatsagar)** updated HTML
   language support
-  `d1aa179 <../../commit/d1aa179>`__ - **(dvenkatsagar)** updated HTML
   language support
-  `89aeecf <../../commit/89aeecf>`__ - **(dvenkatsagar)** Fixed HTML
   language support
-  `0d40bb2 <../../commit/0d40bb2>`__ - **(dvenkatsagar)** Updated
   Version : 0.1.3

0.1.3
^^^^^

-  `d0160df <../../commit/d0160df>`__ - **(dvenkatsagar)** Fixed Minor
   bug. vpat++
-  `611fdc8 <../../commit/611fdc8>`__ - **(dvenkatsagar)** Added Cursor
   Position support
-  `c7a0863 <../../commit/c7a0863>`__ - **(dvenkatsagar)** Updated
   support for html
-  `8206d31 <../../commit/8206d31>`__ - **(dvenkatsagar)** Updated
   Version

0.1.2
^^^^^

-  `7818c11 <../../commit/7818c11>`__ - **(dvenkatsagar)** Added Few
   buttons to editor. vpat++
-  `d2a6e79 <../../commit/d2a6e79>`__ - **(dvenkatsagar)** fixed
   language support
-  `2f0411f <../../commit/2f0411f>`__ - **(dvenkatsagar)** fixed
   language support
-  `a509763 <../../commit/a509763>`__ - **(dvenkatsagar)** Update
   Version

0.1.1
^^^^^

-  `b21c3be <../../commit/b21c3be>`__ - **(dvenkatsagar)** Added
   Language Syntax support. vpat++
-  `27b49f4 <../../commit/27b49f4>`__ - **(dvenkatsagar)** Updated
   Version

0.1.0
^^^^^

-  `019d53f <../../commit/019d53f>`__ - **(dvenkatsagar)** Added speech
   recognition support. vmin++
-  `df7d376 <../../commit/df7d376>`__ - **(dvenkatsagar)** Added
   resource HTML syntax elements
-  `760d1db <../../commit/760d1db>`__ - **(dvenkatsagar)** Added Line
   number display to texteditor

0.0.0
^^^^^

-  `8a6b485 <../../commit/8a6b485>`__ - **(dvenkatsagar)** initial
   commit

