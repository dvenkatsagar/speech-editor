Speech-to-program project
=========================

A simple editor that allows the user to develop programs using their voice.

**Working example** : https://dvenkatsagar.github.io/proj/stp/index.html

Requirements
============
- Chrome Browser with speech recognition support.
- Windows/Linux
- local server

Get it from the source
======================

>>> git clone https://gitlab.com/dvenkatsagar/speech-editor.git
>>> cd speech-editor
>>> python3 server.py

Contact Us
==========
If you guys encounter any bugs, or issues. please contact me via email:

:Authors: Sagar D V <dvenkatsagar@gmail.com>

:Version: 0.1.6 (alpha)
